#!/usr/bin/env python3

de = ''
def ping(srv):
    ''' ping a srv and collect disp '''

    import subprocess as s
    import re

    out = s.run('ping -c1 -W1 -w1 ' + srv, shell=True, stdout=s.PIPE, stderr=s.PIPE).stdout.decode()
    if out.find('0 received') != -1:
        return None

    global de
    de = out
    search = re.search('time=[0-9]+\.[0-9]+', out)
    if search is None:
        search = re.search('time=[0-9]+', out)
        if search is None:
            return None
    time = search.group(0)
    return int(float(time.split('=')[1]) * 1000)


def main(scr):
    import curses
    import os
    import time
    import sys

    if len(sys.argv) != 2:
        raise Exception('What server do you want to ping?')

    if curses.has_colors() == False:
        raise Exception('No color!')

    srv = sys.argv[1]
    height, width = scr.getmaxyx()
    curses.start_color()
    curses.use_default_colors()
    curses.init_pair(1, curses.COLOR_GREEN, curses.COLOR_GREEN)
    curses.init_pair(2, curses.COLOR_YELLOW, curses.COLOR_YELLOW)
    curses.init_pair(3, curses.COLOR_RED, curses.COLOR_RED)
    curses.init_pair(4, curses.COLOR_WHITE, curses.COLOR_BLACK)
    curses.init_pair(5, curses.COLOR_BLACK, curses.COLOR_WHITE)
    curses.init_pair(6, curses.COLOR_BLUE, -1)
    curses.init_pair(7, curses.COLOR_BLUE, curses.COLOR_GREEN)
    curses.init_pair(8, curses.COLOR_BLUE, curses.COLOR_YELLOW)
    curses.init_pair(9, curses.COLOR_BLUE, curses.COLOR_RED)
    try:
        curses.curs_set(0)
    except curses.error:
        pass

    scr.nodelay(True)
    pos = 0
    disp = [{'ping': 0, 'avg': 0} for x in range(0, width)]
    stats = {
        'max': 0,
        'min': 100000000000,
        'cnt': 0,
        'lost': 0,
        'avg': 0,
        'cur': 0
    }
    mwidth = 19
    while True:
        nh, nw = scr.getmaxyx()
        if nh != height or nw != width:
            height = nh
            width = nw
            disp = [{'ping': 0, 'avg': 0} for x in range(0, width)]
            if pos > len(disp):
                pos = 0

        if width < mwidth:
            raise Exception('Not enough columns to draw summary window. Make the terminal larger.')
        if height < 5:
            raise Exception('Not enough rows to draw summary window. Make the terminal larger.')

        pin = ping(srv)
        if pin is None:
            stats['lost'] += 1
            stats['cur'] = -1
        else:
            stats['cur'] = pin
            stats['cnt'] += 1
            if stats['min'] > pin:
                stats['min'] = pin
            if stats['max'] < pin:
                stats['max'] = pin

        lmax = 0
        for p in disp:
            if p['ping'] > lmax:
                lmax = p['ping']

        if pin is None:
            continue

        disp[pos]['ping'] = pin
        disp[pos]['avg'] = (disp[pos - 1]['avg'] if disp[pos - 1]['avg'] != 0 else pin) * 0.95 + pin * 0.05
        stats['avg'] = disp[pos]['avg']

        scr.erase()
        for idx, p in enumerate(disp):
            y = int(min(max(min(p['ping'], lmax) / lmax * height, 0), height)) if lmax != 0 else 0
            color = 1 if p['ping'] < 100000 else 2 if p['ping'] < 500000 else 3

            # last column and row cannot be written to for whatever reason (a curses thing)
            for i in range(0, y):
                try:
                    scr.addstr(height - i - 1, idx, '+', curses.color_pair(color))
                except curses.error:
                    pass

            ay = int(min(max(min(p['avg'], lmax) / lmax * height, 0), height - 1)) if lmax != 0 else 0
            color = 6 if y < ay else color + 6

            try:
                if ay > 0:
                    scr.addch(height - ay - 1, idx, curses.ACS_CKBOARD, curses.color_pair(color))
            except curses.error:
                pass

        for i in range(pos + 1, pos + 3):
            ii = i if i < width else i - width
            for j in range(0, height):
                try:
                    scr.addstr(j, ii, ' ')
                except curses.error:
                    pass

        avg = stats['avg']
        maxi = stats['max']
        mini = stats['min']
        cnt = min(999999, stats['cnt'])
        lcnt = min(999999, stats['lost'])
        unit = 'us'
        if pin > 999:
            unit = 'ms'
            mini /= 1000
            maxi /= 1000
            pin /= 1000
            avg /= 1000

        dsrv = srv if len(srv) <= mwidth - 4 else srv[:mwidth - 4]
        r = int(height * 0.75 - 2)
        c = int(width / 2 - mwidth / 2)
        scr.addstr(
            r, c,
            '{0:{mw}}({1})'.format(dsrv, unit, mw=(mwidth - 4)),
            curses.color_pair(5)
        )
        scr.addstr(
            r + 1, c,
            'Min{0:6.0f}'.format(mini)
            + ' CLs{0:6.0f}'.format(lcnt),
            curses.color_pair(4)
        )
        scr.addstr(
            r + 2, c,
            'Max{0:6.0f}'.format(maxi)
            + ' Cnt{0:6.0f}'.format(cnt),
            curses.color_pair(4)
        )
        scr.addstr(
            r + 3, c,
            'Avg{0:6.0f}'.format(avg)
            + ' Cur{0:6.0f}'.format(pin),
            curses.color_pair(4)
        )

        #scr.addstr(0,0, de)

        pos += 1
        if pos >= width:
            pos = 0

        scr.refresh()
        if scr.getch() == 113: # 'q'
            break

        time.sleep(.9)


if __name__ == "__main__":
    from curses import wrapper
    wrapper(main)
