# ping

Visual ping for use in your favorite terminal.

### Usage
`$ ping.py [IP or DN]`

### Screenshot
![](doc/ping.png)